<?php

class Produto implements IProduto
{
	private $id,$nome,$estoque,$valor;

	public function __construct($id, $nome, $estoque, $valor)
	{
		$this->id = $id;
		$this->nome = $nome;
		$this->estoque = $estoque;
		$this->valor = $valor;
	}
}