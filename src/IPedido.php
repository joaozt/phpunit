<?php

interface IPedido
{
	public function getPedidoItens();

	public function setProduto(IProduto $object_produto);

	public function salvar();
}