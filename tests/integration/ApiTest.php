<?php

class ApiTest extends PHPUnit_Framework_TestCase
{
	private $client;

	public function setUp()
	{
		// Arange (preparar dados)
		$this->client = new GuzzleHttp\Client([
			'base_uri' => 'http://localhost.grupow.com.br/'
		]);
	}

	public function testChamarPedidoEReceber200SucessoListaVazia()
	{
		// Arrange
		$resposta = $this->client->get(
			"qualister-php-testing/api/pedido",
			['auth' => ['phptesting', '123']]
		);

		// Act
		$body = json_decode($resposta->getBody(), true);

		// Assets
		$this->assertEquals(200, $resposta->getStatusCode());
		$this->assertEquals("application/json", $this->_getContentType($resposta->getHeader('content-type')));
		$this->assertEquals("successo", $body["status"]);
		$this->assertEquals("A lista está vazia", $body["message"]);
		$this->assertCount(0, $body["data"]);
	}

	public function testAcessarRootEntaoReceber200SucessoECodigo()
	{
		// Act (executar acoes)
		$resposta = $this->client->get(
			'qualister-php-testing/api/',
			[
			'auth' => ['phptesting', '123']
			]
			);

		// Assert (adicionar os asserts de validacao)
		$this->assertEquals(200, $resposta->getStatusCode());
		$this->assertEquals('application/json', $this->_getContentType($resposta->getHeader('content-type')));

		$body = json_decode($resposta->getBody(), true);

		$this->assertTrue((isset($body['status']) && $body['status'] == 'sucesso' ?: false));
	}

	public function testChamarPedido1EReceber200SucessoNomeECodigoCliente()
	{
		// Act
		$resposta = $this->client->get(
			'qualister-php-testing/api/pedido/1?clientenome=Joao',
			['auth' => ['phptesting', '123']
		]);

		// Asserts
		$this->assertEquals(200, $resposta->getStatusCode());
		$this->assertEquals("application/json", $this->_getContentType($resposta->getHeader('content-type')));
		$body = json_decode($resposta->getBody(), true);
		$this->assertTrue((isset($body['status']) && $body['status'] == 'sucesso' ?: false));
		$this->assertEquals("Seu código é 1", $body["message"]);
		$this->assertEquals("Joao", $body["data"]["clientenome"]);
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testAdicionarItemAoPedidoERetornarSucesso($id1, $nome1, $qnde1, $valor1, $id2, $nome2, $qnde2, $valor2)
	{
		// Arrange
		$resposta = $this->client->post(
			"qualister-php-testing/api/pedido",
			[
				'auth' => ['phptesting', '123'],
				'form_params' => [
					'produtoid' => $id1,
					'produtonome' => $nome1,
					'produtoestoque' => $qnde1,
					'produtovalor' => $valor1,
					'quantidade' => 10
				],
			]
		);

		// Act
		$body = json_decode($resposta->getBody(), true);

		// Asserts
		$this->assertEquals("Sucesso", $body["message"]);
	}

	public function testValidarUmSchemaJson()
	{
		// Arrange
		$resposta = $this->client->get(
			"qualister-php-testing/api/",
			['auth' => ['phptesting', '123']]
		);
		
		// Act
		$validator = new Json\Validator('tests/integration/schema/schema.json');
		
		// Assert
		$validator->validate(json_decode($resposta->getBody()));
	} 

	public function dataProvider()
	{
		return array_map('str_getcsv', file('tests/unit/ddt/dataProvider.csv'));
	}

	private function _getContentType($header)
	{
		return current(($header) ?: array());
	}
}