<?php

class PedidoTest extends PHPUnit_Framework_TestCase
{
	private $pedido;

	public static function setUpBeforeClass(){}

	/**
	 * @cover Pedido
	 */
	protected function setUp()
	{
		// Arrange
		$this->pedido = new Pedido();
	}

	/**
	 * @test
	 * @group Importantes
	 * @author Joao Moraes
	 * @small
	 * @cover Pedido
	 * @cover Pedido::getPedidoItens
	 */
	public function listaDePedidosDeveConter0Itens()
	{
		// Action
		$pedidoitens = $this->pedido->getPedidoItens();

		// Assert
		$this->assertCount(0, $pedidoitens);
	}

	/**
	 * @group testao
	 * @cover Produto
	 * @cover Pedido
	 * @cover Pedido::setProduto
	 * @cover Pedido::getPedidoItens
	 * @dataProvider dataProvider
	 */
	public function testAdicionar2ProdutosEntaoAListaDeveConter2Itens($id1, $nome1, $qnde1, $valor1, $id2, $nome2, $qnde2, $valor2)
	{
		// Arrange
		$produto_1 = $this->getMockBuilder('IProduto')
						  ->setConstructorArgs(array($id1, $nome1, $qnde1, $valor1))
						  ->getMock();

		$produto_2 = $this->getMockBuilder('IProduto')
						  ->setConstructorArgs(array($id2, $nome2, $qnde2, $valor2))
						  ->getMock();

		// Act
		$this->pedido->setProduto($produto_1);
		$this->pedido->setProduto($produto_2);

		// Assert
		$this->assertCount(2, $this->pedido->getPedidoItens());
	}

	public function dataProvider()
	{
		return array_map('str_getcsv', file('tests/unit/ddt/dataProvider.csv'));
	}

	/**
	 * @expectedException Exception
	 * @cover Exception
	 */
	public function testLancarExceptionComUmaMensagemCodigo20()
	{
		// Act
		if(1 == 1){
			throw new erro("Uma mensagem", 20);
		}else{
			throw new Exception("Uma mensagem", 20);
		}
	}

	protected function tearDown()
	{
		unset($this->pedido);
	}

	/**
	 * @cover Produto
	 * @cover Pedido
	 * @cover Pedido::setProduto
	 * @cover Pedido::salvar
	 */
	public function testAdicionar2ProdutosSalvarPedidoEReceberMensagemDeSucesso()
	{
		// Arrange
		$produto_1 = $this->getMockBuilder('IProduto')
						  ->setConstructorArgs(array(1, 'Primeiro Produto', 100, 15.50))
						  ->getMock();

		$produto_2 = $this->getMockBuilder('IProduto')
						  ->setConstructorArgs(array(2, 'Segundo Produto', 50, 10.99))
						  ->getMock();

		// Action
		$this->pedido->setProduto($produto_1);
		$this->pedido->setProduto($produto_2);
		$status = $this->pedido->salvar($this->pedido);
		
		// Assert
		$this->assertSame('3', '3');

	}
}

class erro extends Exception
{

}