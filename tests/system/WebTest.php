<?php

class WebTest extends PHPUnit_Framework_TestCase
{
	private
		$driver,
		$url = 'http://localhost/qualister-php-testing/web/',
		$url_web = 'http://localhost.grupow.com.br/qualister-php-testing/web/';

	public function setUp()
	{
		// Arrange
		$this->driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			DesiredCapabilities::firefox()
			);

		// Act
		$this->driver->manage()->window()->maximize();
	}

	public function testAcessarPaginaInicialEVerMensagemBemVindo()
	{
		// Act
		$this->driver->get($this->url);
	}

	public function testRealizarNavegacoesEEntaoFecharOBrowser()
	{
		// Act
		$this->driver->get($this->url);
		$url = $this->url . 'listaritens.php';
		$this->driver->navigate()->to($url);
		$this->driver->navigate()->refresh();
		$this->driver->navigate()->back();
		$this->driver->navigate()->forward();
	}

	public function testAcessarPaginaInicialEApresentarTextoDosElementos()
	{
		// Act
		$this->driver->get($this->url);
		$title = $this->driver->getTitle();
		$title_logo = $this->driver->findElement(WebDriverBy::id("logo-container"))->getText();
		$title_h1 = $this->driver->findElement(WebDriverBy::tagName("h1"))->getText();
		$tag_listaitens = $this->driver->findElement(WebDriverBy::id("listaritens"))->getTagName();

		// Asserts
		$this->assertEquals('Starter Template - Materialize', $title);
		$this->assertEquals('Qualister PHP Testing', $title_logo);
		$this->assertEquals('Bem vindos a API Qualister', $title_h1);
		$this->assertEquals('a', $tag_listaitens);

	}

	public function testAcessarPaginaInicialEApresentarTextoUsandoCSS()
	{
		// Arrange
		$this->driver->get($this->url);
		
		// Act
		$footer = $this->driver->findElement(
			WebDriverBy::cssSelector(".footer-copyright > .container")
			)->getText();
		$tag = $this->driver->findElement(
			WebDriverBy::cssSelector("*[class*='no-pad-bot']")
			)->getTagName();
		
		// Assert
		$this->assertEquals("Made by Materialize", $footer);
		$this->assertEquals("div", $tag);
	}

	public function testAcessarPaginaInicialEApresentarTextoUsandoXPath()
	{
		// Arrange
		$this->driver->get($this->url);

		// Act
		$banner = $this->driver->findElement(
			WebDriverBy::xpath("//*[@id='index-banner']/*/*")
			)->getText();
		
		$href = $this->driver->findElement(
			WebDriverBy::xpath("//li[1]/a")
			)->getAttribute("href");

		$white = $this->driver->findElement(
			WebDriverBy::xpath("//h5[@class='white-text']")
			)->getText();
		
		// Assert
		$this->assertEquals("Bem vindos a API Qualister", $banner);
		$this->assertContains("listaritens.php", $href);
		$this->assertEquals("Company Bio", $white);
	}

	public function testAcessarPaginaInicialClicarEmMeusPedidosMostrarAlerta()
	{
		// Arrange
		$this->driver->get($this->url);
		
		// Act
		$this->driver->findElement(WebDriverBy::id("meuspedidos"))->click();
		$this->driver->switchTo()->alert()->accept();
		$texto = $this->driver->switchTo()->alert()->getText();
		$this->driver->switchTo()->alert()->dismiss();
		
		// Assert
		$this->assertEquals("Faça login antes", $texto);
	}

	public function testCriarNovoPedidoEReceberMensagemSucesso()
	{
		// Arrange
		$this->driver->get($this->url_web);
		
		// Act
		$this->driver->findElement(WebDriverBy::id("novopedido"))->click();
		$this->driver->findElement(WebDriverBy::id("id"))->sendKeys(15);
		$combo = $this->driver->findElement(WebDriverBy::id("produto"));
		$combo = new WebDriverSelect($combo);
		$combo->selectByValue("Firefox");
		$this->driver->findElement(WebDriverBy::id("estoque"))->sendKeys(100);
		$this->driver->findElement(WebDriverBy::id("valor"))->sendKeys(19.90);
		$this->driver->findElement(WebDriverBy::cssSelector("label[for='quantidade5']"))->click();
		$this->driver->findElement(WebDriverBy::tagName("button"))->click();
		$mensagem = $this->driver->switchTo()->alert()->getText();
		$this->driver->switchTo()->alert()->dismiss();
		
		// Arrange
		$this->assertEquals("Sucesso", $mensagem);
	}

	protected function tearDown()
	{
		// Act
		$this->driver->quit();
	}
}